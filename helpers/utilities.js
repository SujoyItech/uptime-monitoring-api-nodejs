const utilities = {};
const crypto = require('crypto');
const environment = require('./environment');

// Parse json to object

utilities.parseJson = (jsonString) => {
    let output = {};
    try {
        output = JSON.parse(jsonString);
    } catch {
        console.log('Data type error!');
    }
    return output;
};

utilities.hash = (str) => {
    if (typeof str === 'string' && str.length) {
        const hash = crypto.createHmac('sha256', environment.secretKey).update(str).digest('hex');
        return hash;
    }
    return false;
};

utilities.createRandomString = (strLength) => {
    const length = typeof strLength === 'number' && strLength > 0 ? strLength : false;
    if (length) {
        const possibleCharacters = 'abcdefghijklmnopqrstuvwxyz123456789';
        let output = '';
        for (let i = 1; i <= length; i++) {
            const raddomCharacter = possibleCharacters.charAt(
                Math.floor(Math.random() * possibleCharacters.length)
            );
            output += raddomCharacter;
        }
        return output;
    }
    return false;
};

module.exports = utilities;
