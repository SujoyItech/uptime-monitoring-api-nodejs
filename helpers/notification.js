const https = require('https');
const querystring = require('querystring');
const { twilio } = require('./environment');

const notifications = {};

notifications.sendTwilioSms = (phone, message, callback) => {
    const userPhone = typeof phone === 'string' && phone.length === 11 ? phone : false;
    const userMsg = typeof message === 'string' && message.length > 0 ? message : false;
    if (userPhone && userMsg) {
        const payload = {
            From: twilio.fromPhone,
            To: `+88${userPhone}`,
            Body: userMsg,
        };
        const payloadString = querystring.stringify(payload);
        const requestDetails = {
            hostname: 'api.twilio.com',
            method: 'POST',
            path: `/2010-04-01/Accounts/${twilio.accountSid}/Messages.json`,
            auth: `${twilio.accountSid}:${twilio.authToken}`,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
        };

        const req = https.request(requestDetails, (res) => {
            const status = res.statusCode;
            if (status === 200 || status === 201) {
                callback(false);
            } else {
                callback(`Status code return was ${status}`);
            }
        });
        req.on('error', (e) => {
            callback(e);
        });
        req.write(payloadString);
        req.end();
    } else {
        callback('Given parameter was missing or invalid!');
    }
};

module.exports = notifications;
