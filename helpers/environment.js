const environments = {};
environments.staging = {
    port: 3000,
    envName: 'staging',
    secretKey: 'sfhsfsdfsfdasdfsfsfsd',
    maxChecks: 5,
    twilio: {
        fromPhone: '+16175551212',
        accountSid: 'AC02efc20717ab7d4d2efab013fda80f7f',
        authToken: 'a04394a0f611cab6992f0ae327d8cb20',
    },
};

environments.production = {
    port: 5000,
    envName: 'production',
    secretKey: 'sdfsdfsfssghhfgfhhdfff',
    maxChecks: 5,
    twilio: {
        fromPhone: '+16175551212',
        accountSid: 'AC02efc20717ab7d4d2efab013fda80f7f',
        authToken: 'a04394a0f611cab6992f0ae327d8cb20',
    },
};

const currentEnvironment =
    typeof process.env.NODE_ENV === 'string' ? typeof process.env.NODE_ENV : 'staging';

const environmentToExport =
    typeof environments[currentEnvironment] === 'object'
        ? environments[currentEnvironment]
        : environments.staging;

module.exports = environmentToExport;
