const handler = {};

const data = require('../../lib/data');
const { hash } = require('../../helpers/utilities');
const { createRandomString } = require('../../helpers/utilities');
const { parseJson } = require('../../helpers/utilities');

handler.tokenHandler = (requestProperties, callback) => {
    const acceptedMethods = ['get', 'post', 'put', 'delete'];
    if (acceptedMethods.indexOf(requestProperties.method) > -1) {
        handler._token[requestProperties.method](requestProperties, callback);
    } else {
        callback(405);
    }
};
handler._token = {};

handler._token.post = (requestProperties, callback) => {
    const phone =
        typeof requestProperties.body.phone === 'string' &&
        requestProperties.body.phone.trim().length === 11
            ? requestProperties.body.phone
            : false;

    const password =
        typeof requestProperties.body.password === 'string' &&
        requestProperties.body.password.trim().length > 0
            ? requestProperties.body.password
            : false;
    if (phone && password) {
        data.read('users', phone, (err, userData) => {
            if (!err && userData) {
                const hashedpassword = hash(password);
                const user = parseJson(userData);
                if (user.password === hashedpassword) {
                    const tokenId = createRandomString(20);
                    const expires = Date.now() * 60 * 60 * 1000;
                    const tokenObject = {
                        phone,
                        id: tokenId,
                        expires,
                    };
                    data.create('tokens', tokenId, tokenObject, (tokenCreateError) => {
                        if (!tokenCreateError) {
                            callback(200, tokenObject);
                        } else {
                            callback(400, { error: 'Token create failed' });
                        }
                    });
                } else {
                    callback(400, { error: 'Password not matched!' });
                }
            } else {
                callback(400, { error: 'User not found!' });
            }
        });
    } else {
        callback(400, { error: 'Phone or password incorrect!' });
    }
};

handler._token.get = (requestProperties, callback) => {
    const id =
        typeof requestProperties.queryStringObject.id === 'string' &&
        requestProperties.queryStringObject.id.trim().length === 20
            ? requestProperties.queryStringObject.id
            : false;

    if (id) {
        data.read('tokens', id, (err, tokenData) => {
            const token = { ...parseJson(tokenData) };
            if (!err && token) {
                callback(200, { token });
            } else {
                callback(404, { error: 'Token is invalid!' });
            }
        });
    } else {
        callback(404, { error: 'Invalid token id!' });
    }
};

handler._token.put = (requestProperties, callback) => {
    const id =
        typeof requestProperties.body.id === 'string' &&
        requestProperties.body.id.trim().length === 20
            ? requestProperties.body.id
            : false;

    const extend =
        typeof requestProperties.body.extend === 'boolean' &&
        requestProperties.body.extend === true;

    if (id && extend) {
        data.read('tokens', id, (err, tokenData) => {
            if (err) {
                callback(400, { error: 'Token not found!' });
            } else {
                const token = parseJson(tokenData);
                if (token.expires > Date.now()) {
                    token.expires += 60 * 60 * 1000;
                    data.update('tokens', id, token, (updateError) => {
                        if (updateError) {
                            callback(400, { error: 'Token update failed.' });
                        } else {
                            callback(200, { msg: 'Token updated successfully.' });
                        }
                    });
                } else {
                    callback(400, { error: 'Token already expired!' });
                }
            }
        });
    } else {
        callback(400, { error: 'Token is invalid!' });
    }
};

handler._token.delete = (requestProperties, callback) => {
    const id =
        typeof requestProperties.queryStringObject.id === 'string' &&
        requestProperties.queryStringObject.id.trim().length === 20
            ? requestProperties.queryStringObject.id
            : false;

    if (id) {
        // lookup the user
        data.read('tokens', id, (err, tokenData) => {
            if (!err && tokenData) {
                data.delete('tokens', id, (deleteErr) => {
                    if (deleteErr) {
                        callback(400, { error: 'Token deleted failed' });
                    } else {
                        callback(200, { msg: 'Token deleted successfully.' });
                    }
                });
            } else {
                callback(400, { error: 'Token not found!' });
            }
        });
    } else {
        callback(400, { error: 'Token invalid!' });
    }
};

handler._token.verify = (id, phone, callback) => {
    data.read('tokens', id, (error, tokenData) => {
        if (!error && tokenData) {
            const token = parseJson(tokenData);
            if (token.phone === phone && token.expires > Date.now()) {
                callback(true);
            } else {
                callback(false);
            }
        } else {
            callback(false);
        }
    });
};
module.exports = handler;
