const handler = {};
handler.sampleHandler = (requestProperties, callback) => {
    console.log(requestProperties);
    const response = {
        message: 'This is a sample message',
    };
    callback(200, response);
};

module.exports = handler;
