const handler = {};
handler.notFoundHandler = (requestProperties, callback) => {
    console.log(requestProperties);
    const response = {
        message: '404 Not found',
    };

    callback(404, response);
};

module.exports = handler;
