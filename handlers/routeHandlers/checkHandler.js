const handler = {};

const data = require('../../lib/data');
const { parseJson, createRandomString } = require('../../helpers/utilities');
const tokenHandler = require('./tokenHandler');
const { maxChecks } = require('../../helpers/environment');

handler.checkHandler = (requestProperties, callback) => {
    const acceptedMethods = ['get', 'post', 'put', 'delete'];
    if (acceptedMethods.indexOf(requestProperties.method) > -1) {
        handler._check[requestProperties.method](requestProperties, callback);
    } else {
        callback(405);
    }
};
handler._check = {};

handler._check.post = (requestProperties, callback) => {
    const protocol =
        typeof requestProperties.body.protocol === 'string' &&
        ['http', 'https'].indexOf(requestProperties.body.protocol) > -1
            ? requestProperties.body.protocol
            : false;
    const url =
        typeof requestProperties.body.url === 'string' && requestProperties.body.url.length > 0
            ? requestProperties.body.url
            : false;
    const method =
        typeof requestProperties.body.method === 'string' &&
        ['GET', 'POST', 'PUT', 'DELETE'].indexOf(requestProperties.body.method) > -1
            ? requestProperties.body.method
            : false;
    const successCodes =
        typeof requestProperties.body.successCodes === 'object' &&
        requestProperties.body.successCodes instanceof Array
            ? requestProperties.body.successCodes
            : false;
    const timeoutSeconds =
        typeof requestProperties.body.timeoutSeconds === 'number' &&
        requestProperties.body.timeoutSeconds % 1 === 0 &&
        requestProperties.body.timeoutSeconds >= 1 &&
        requestProperties.body.timeoutSeconds <= 5
            ? requestProperties.body.timeoutSeconds
            : false;
    if (protocol && url && method && successCodes && timeoutSeconds) {
        // verify token
        const token =
            typeof requestProperties.headersObject.token === 'string'
                ? requestProperties.headersObject.token
                : false;

        data.read('tokens', token, (err1, tokenData) => {
            if (!err1 && tokenData) {
                const userPhone = parseJson(tokenData).phone;
                data.read('users', userPhone, (err2, userData) => {
                    if (!err2 && userData) {
                        tokenHandler._token.verify(token, userPhone, (tokenId) => {
                            if (tokenId) {
                                // lookup the user
                                const userObjects = parseJson(userData);
                                const userChecks =
                                    typeof userObjects.checks === 'object' &&
                                    userObjects.checks instanceof Array
                                        ? userObjects.checks
                                        : (userObjects.checks = []);
                                if (userChecks.length < maxChecks) {
                                    const checkId = createRandomString(20);
                                    const checkObject = {
                                        id: checkId,
                                        userPhone,
                                        protocol,
                                        url,
                                        method,
                                        successCodes,
                                        timeoutSeconds,
                                    };
                                    data.create('checks', checkId, checkObject, (createError) => {
                                        if (!createError) {
                                            userObjects.checks = userChecks;
                                            userObjects.checks.push(checkId);
                                            data.update(
                                                'users',
                                                userPhone,
                                                userObjects,
                                                (userUpdateError) => {
                                                    if (!userUpdateError) {
                                                        callback(200, checkObject);
                                                    } else {
                                                        callback(500, {
                                                            error: 'There was a problem in server side in updating users!',
                                                        });
                                                    }
                                                }
                                            );
                                        } else {
                                            callback(500, {
                                                error: 'There was a problem in server side in creating checks!',
                                            });
                                        }
                                    });
                                } else {
                                    callback(401, {
                                        error: 'User has already reached max check limit!',
                                    });
                                }
                            } else {
                                callback(403, {
                                    error: 'Authentication failure!',
                                });
                            }
                        });
                    } else {
                        callback(403, { error: 'User not found!' });
                    }
                });
            } else {
                callback(403, { error: 'Authentication error!' });
            }
        });
    } else {
        callback(400, { error: 'Validation failed!' });
    }
};

handler._check.get = (requestProperties, callback) => {
    const id =
        typeof requestProperties.queryStringObject.id === 'string' &&
        requestProperties.queryStringObject.id.trim().length === 20
            ? requestProperties.queryStringObject.id
            : false;

    if (id) {
        data.read('checks', id, (err1, checkData) => {
            if (!err1 && checkData) {
                const token =
                    typeof requestProperties.headersObject.token === 'string'
                        ? requestProperties.headersObject.token
                        : false;
                const checkObject = parseJson(checkData);
                tokenHandler._token.verify(token, checkObject.userPhone, (tokenId) => {
                    if (tokenId) {
                        callback(200, checkObject);
                    } else {
                        callback(403, { error: 'Authentication failed!' });
                    }
                });
            } else {
                callback(500, { error: 'Server error while reading check data!' });
            }
        });
    } else {
        callback(400, { error: 'Check data not found by id!' });
    }
};

handler._check.put = (requestProperties, callback) => {
    const id =
        typeof requestProperties.body.id === 'string' &&
        requestProperties.body.id.trim().length === 20
            ? requestProperties.body.id
            : false;

    const protocol =
        typeof requestProperties.body.protocol === 'string' &&
        ['http', 'https'].indexOf(requestProperties.body.protocol) > -1
            ? requestProperties.body.protocol
            : false;
    const url =
        typeof requestProperties.body.url === 'string' && requestProperties.body.url.length > 0
            ? requestProperties.body.url
            : false;
    const method =
        typeof requestProperties.body.method === 'string' &&
        ['GET', 'POST', 'PUT', 'DELETE'].indexOf(requestProperties.body.method) > -1
            ? requestProperties.body.method
            : false;
    const successCodes =
        typeof requestProperties.body.successCodes === 'object' &&
        requestProperties.body.successCodes instanceof Array
            ? requestProperties.body.successCodes
            : false;
    const timeoutSeconds =
        typeof requestProperties.body.timeoutSeconds === 'number' &&
        requestProperties.body.timeoutSeconds % 1 === 0 &&
        requestProperties.body.timeoutSeconds >= 1 &&
        requestProperties.body.timeoutSeconds <= 5
            ? requestProperties.body.timeoutSeconds
            : false;
    if (id) {
        if (protocol || url || method || successCodes || timeoutSeconds) {
            data.read('checks', id, (err, checkData) => {
                if (!err && checkData) {
                    const checkObject = parseJson(checkData);
                    const token =
                        typeof requestProperties.headersObject.token === 'string'
                            ? requestProperties.headersObject.token
                            : false;

                    tokenHandler._token.verify(token, checkObject.userPhone, (tokenId) => {
                        if (tokenId) {
                            if (protocol) {
                                checkObject.protocol = protocol;
                            }
                            if (url) {
                                checkObject.url = url;
                            }
                            if (method) {
                                checkObject.method = method;
                            }
                            if (successCodes) {
                                checkObject.successCodes = successCodes;
                            }
                            if (timeoutSeconds) {
                                checkObject.timeoutSeconds = timeoutSeconds;
                            }

                            data.update('checks', id, checkObject, (err2) => {
                                if (!err2) {
                                    callback(200, { msg: 'Check check updated successfully' });
                                } else {
                                    callback(500, {
                                        error: 'Server error while updating check data',
                                    });
                                }
                            });
                        } else {
                            callback(403, { error: 'Authentication failed!' });
                        }
                    });
                } else {
                    callback(500, 'Server error while reading checks');
                }
            });
        } else {
            callback(400, { error: 'Nothing is updated.' });
        }
    }
};

handler._check.delete = (requestProperties, callback) => {
    const id =
        typeof requestProperties.queryStringObject.id === 'string' &&
        requestProperties.queryStringObject.id.trim().length === 20
            ? requestProperties.queryStringObject.id
            : false;

    if (id) {
        data.read('checks', id, (err1, checkData) => {
            if (!err1 && checkData) {
                const token =
                    typeof requestProperties.headersObject.token === 'string'
                        ? requestProperties.headersObject.token
                        : false;
                const checkObject = parseJson(checkData);
                tokenHandler._token.verify(token, checkObject.userPhone, (tokenId) => {
                    if (tokenId) {
                        data.delete('checks', id, (err2) => {
                            if (!err2) {
                                data.read('users', checkObject.userPhone, (err3, userData) => {
                                    const userObjects = parseJson(userData);
                                    if (!err3 && userData) {
                                        const userChecks =
                                            typeof userObjects.checks === 'object' &&
                                            userObjects.checks instanceof Array
                                                ? userObjects.checks
                                                : [];
                                        const checkPosition = userObjects.indexOf(id);
                                        if (checkPosition > -1) {
                                            userChecks.splice(checkPosition, 1);
                                            userObjects.checks = userChecks;
                                            data.update(
                                                'users',
                                                checkObject.userPhone,
                                                userObjects,
                                                (err4) => {
                                                    if (!err4) {
                                                        callback(200, {
                                                            msg: 'Check deleted successfully.',
                                                        });
                                                    } else {
                                                        callback(200, {
                                                            msg: 'User updated failed.',
                                                        });
                                                    }
                                                }
                                            );
                                        } else {
                                            callback(400, {
                                                error: 'No check object found in user.',
                                            });
                                        }
                                    } else {
                                        callback(500, {
                                            error: 'Server error while getting user.',
                                        });
                                    }
                                });
                                callback(200, { msg: 'Check data deleted successfully.' });
                            } else {
                                callback(500, { error: 'Server error while deleting check data' });
                            }
                        });
                    } else {
                        callback(403, { error: 'Authentication failed!' });
                    }
                });
            } else {
                callback(500, { error: 'Server error while reading check data!' });
            }
        });
    } else {
        callback(400, { error: 'Check data not found by id!' });
    }
};
module.exports = handler;
