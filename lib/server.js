/*
Title : Uptime monitoring application
Description: A resful api to monitor up and down link
Author: Sujoy Nath
Date:
*/

// dependency

const http = require('http');

const environments = require('../helpers/environment');

const { handleReqRes } = require('../helpers/handleReqRes');

const server = {};
server.createServer = () => {
    const createServer = http.createServer(server.handleReqRes);
    createServer.listen(environments.port, () => {
        console.log(`Listenting to port ${environments.port}...`);
    });
};

server.handleReqRes = handleReqRes;

server.init = () => {
    server.createServer();
};

module.exports = server;
