/*
Title : Uptime monitoring application
Description: A resful api to monitor up and down link
Author: Sujoy Nath
Date:
*/
const url = require('url');
const http = require('http');
const https = require('https');
const data = require('./data');
const { parseJson } = require('../helpers/utilities');
const { sendTwilioSms } = require('../helpers/notification');

// dependency

const worker = {};

worker.gatherAllTheCheck = () => {
    data.list('checks', (err, checkData) => {
        if (!err && checkData) {
            checkData.array.forEach((check) => {
                data.read('checks', check, (err2, originalCheckData) => {
                    if (!err2 && originalCheckData) {
                        worker.validateCheckData(parseJson(originalCheckData));
                    } else {
                        console.log(err2);
                    }
                });
            });
        } else {
            console.log(err);
        }
    });
};

worker.validateCheckData = (originalCheckData) => {
    const originalData = originalCheckData;
    if (originalData && originalData.id) {
        originalData.state =
            typeof originalCheckData.state === 'string' &&
            ['up', 'down'].indexOf(originalCheckData.state) > -1
                ? originalCheckData.state
                : 'down';
        originalData.lastChecked =
            typeof originalCheckData.lastChecked === 'number' && originalCheckData.lastChecked > 0
                ? originalCheckData.state
                : false;
        worker.performCheck(originalCheckData);
    } else {
        console('Error check was invalid');
    }
};

worker.performCheck = (originalCheckData) => {
    let checkOutCome = {
        error: false,
        responseCode: false,
    };

    let outcomeSend = false;

    const parsedUrl = url.parse(`${originalCheckData.protocol}://${originalCheckData.url}`, true);
    const { hostname } = parsedUrl;
    const { path } = parsedUrl;

    const requestDetails = {
        protocol: `${originalCheckData.protocol}:`,
        hostname,
        method: originalCheckData.method.toUpperCase(),
        path,
        timeout: originalCheckData.timeoutSeconds * 1000,
    };

    const protocolToUse = originalCheckData.protocol === 'https' ? https : http;

    const req = protocolToUse.request(requestDetails, (res) => {
        const status = res.statusCode;
        checkOutCome.responseCode = status;
        if (!outcomeSend) {
            worker.processCheckOutCome(originalCheckData, checkOutCome);
            outcomeSend = true;
        }
    });

    req.on('error', (e) => {
        if (!outcomeSend) {
            checkOutCome = {
                error: true,
                value: e,
            };
            worker.processCheckOutCome(originalCheckData, checkOutCome);
            outcomeSend = true;
        }
    });

    req.on('timeout', (e) => {
        if (!outcomeSend) {
            checkOutCome = {
                error: true,
                value: 'timeout',
            };
            worker.processCheckOutCome(originalCheckData, checkOutCome);
            outcomeSend = true;
        }
    });
};

worker.processCheckOutCome = (originalCheckData, checkOutCome) => {
    const state =
        !checkOutCome.error &&
        checkOutCome.responseCode &&
        originalCheckData.successCodes.indexOf(checkOutCome.responseCode) > -1
            ? 'up'
            : 'down';
    const alertWanted = !!(originalCheckData.lastChecked && originalCheckData.state !== state);

    const newCheckData = originalCheckData;
    newCheckData.state = state;
    newCheckData.lastChecked = Date.now();
    data.update('checks', newCheckData.id, newCheckData, (err) => {
        if (!err) {
            if (alertWanted) {
                worker.alertUserToStatusChange(newCheckData);
            } else {
                console.log('Alert is not needed as there is not state change.');
            }
        } else {
            console.log('Error trying to update checks.');
        }
    });
};

worker.alertUserToStatusChange = (newCheckData) => {
    const msg = `Alert: You check for ${newCheckData.method.toUpperCase} ${newCheckData.protocol}://${newCheckData.url} is currently ${newCheckData.state}`;
    sendTwilioSms(newCheckData.userPhone, msg, (err) => {
        if (!err) {
            console.log(`User alerted to a state change sms ${msg}`);
        } else {
            console.log('There was as problem in sending sms to user.');
        }
    });
};

worker.alertUserToStatusChange = () => {};

worker.loop = () => {
    setInterval(() => {
        worker.gatherAllTheCheck();
    }, 5000);
};

worker.init = () => {
    console.log('Worker started..');
    worker.gatherAllTheCheck();
    worker.loop();
};

module.exports = worker;
